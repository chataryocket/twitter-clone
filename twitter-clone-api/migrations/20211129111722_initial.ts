import { Knex } from "knex";
import { hashPasswordAsync } from "../src/helper/security-helper";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("login_info", (table) => {
    table.increments("id");
    table.string("email", 50).unique();
    table.string("password");
    table.string("name");
    table.datetime("created_at");
  });

  await knex.schema.createTable("role", (table) => {
    table.increments("id");
    table.string("name");
    table.string("description");
    table.datetime("created_at");
  });

  await knex.schema.createTable("users_in_roles", (table) => {
    table.integer("user_id").references("id").inTable("login_info");
    table.integer("role_id").references("id").inTable("role");
    table.primary(["user_id", "role_id"]);
  });

  var roles = await knex("role").insert(
    [
      {
        name: "admin",
        created_at: new Date(),
      },
    ],
    "id"
  );

  var users = await knex("login_info").insert(
    [
      {
        email: "abc@mail.com",
        password: await hashPasswordAsync("pass@word1"),
        name: "abc xyz",
        created_at: new Date(),
      },
    ],
    "id"
  );

  await knex("users_in_roles").insert([
    {
      user_id: users[0],
      role_id: roles[0],
    },
  ]);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("login_info");
  await knex.schema.dropTable("role");
  await knex.schema.dropTable("users_in_roles");
}
