import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("tweets", (table) => {
    table.increments("id");
    table.string("tweet", 280);
    table.integer("user_id").references("id").inTable("login_info");
    table.datetime("created_at");
  });

  await knex.schema.createTable("follows", (table) => {
    table.increments("id");
    table.integer("follower_id").references("id").inTable("login_info");
    table.integer("following_id").references("id").inTable("login_info");
    table.datetime("created_at");
    table.unique(["follower_id", "following_id"]);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("tweets");
  await knex.schema.dropTable("follows");
}
