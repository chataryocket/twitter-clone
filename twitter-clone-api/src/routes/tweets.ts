import { Router } from "express";
import { Request, Response, NextFunction } from "express";

import TweetModel from "../model/tweets";
import FollowModel from "../model/follwos";
const router = Router();

router.get(
  "",
  async function (req: Request, res: Response, next: NextFunction) {
    const tweets = await TweetModel.query()
      .join("login_info as li", "li.id", "tweets.user_id")
      .where("tweets.user_id", parseInt(req.body.user.id))
      .orWhereIn(
        "tweets.user_id",
        FollowModel.query()
          .select("following_id")
          .where("follower_id", req.body.user.id)
      )
      .select("tweets.*", "li.name as created_by")
      .orderBy("tweets.created_at", "DESC");
    return res.json(tweets);
  }
);

router.post(
  "",
  async function (req: Request, res: Response, next: NextFunction) {
    const tweetRes = await TweetModel.query().insert({
      tweet: req.body.tweet,
      user_id: req.body.user.id,
      created_at: new Date(),
    });
    return res.json(tweetRes);
  }
);

export default router;
