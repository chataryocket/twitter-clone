import { Router } from "express";
import auth from "../helper/auth-helper";

import loginRouter from "./login";
import userRouter from "./user";
import tweetRouter from "./tweets";

const router = Router();
router.use("/api/account", loginRouter);
router.use("/api/user", auth, userRouter);
router.use("/api/tweet", auth, tweetRouter);

export default router;
