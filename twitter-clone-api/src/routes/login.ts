import { Router } from "express";
import StatusCodes from "http-status-codes";
import { Request, Response, NextFunction } from "express";

import {
  comparePasswordAsync,
  createToken,
  hashPasswordAsync,
} from "../helper/security-helper";
import LoginInfoModel from "../model/login-info";
const router = Router();

router.post(
  "/signup",
  async function (req: Request, res: Response, next: NextFunction) {
    const user = await LoginInfoModel.query().findOne(
      "email",
      req.body.username
    );
    if (user) {
      return res.status(StatusCodes.OK).send({
        success: false,
        error: "User with the specified email/phone already exists",
      });
    }

    const users = await LoginInfoModel.query().insert({
      email: req.body.username,
      password: await hashPasswordAsync(req.body.password),
      name: req.body.name,
      created_at: new Date(),
    });

    return res.json({ success: true, error: null });
  }
);

router.post(
  "/login",
  async function (req: Request, res: Response, next: NextFunction) {
    const user = await LoginInfoModel.query().findOne(
      "email",
      req.body.username
    );
    if (!user) {
      return res.status(StatusCodes.NOT_FOUND).send({
        error: StatusCodes.getStatusText(StatusCodes.NOT_FOUND),
      });
    }
    if (user) {
      let comparePassword = await comparePasswordAsync(
        req.body.password,
        user["password"]
      );
      if (!comparePassword) {
        return res.status(StatusCodes.BAD_REQUEST).send({
          error: StatusCodes.getStatusText(StatusCodes.BAD_REQUEST),
        });
      }
    }

    return res.json({ token: createToken(user) });
  }
);

export default router;
