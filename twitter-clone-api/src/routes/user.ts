import { Router } from "express";
import StatusCodes from "http-status-codes";
import { Request, Response, NextFunction } from "express";
import { raw } from "objection";

import LoginInfoModel from "../model/login-info";
import FollowModel from "../model/follwos";
const router = Router();

router.get(
  "",
  async function (req: Request, res: Response, next: NextFunction) {
    const user = await LoginInfoModel.query().findOne(
      "id",
      parseInt(req.body.user.id)
    );
    if (user) {
      delete user.password;
      return res.json(user);
    }
    return res
      .status(StatusCodes.NOT_FOUND)
      .json(StatusCodes.getStatusText(StatusCodes.NOT_FOUND));
  }
);

router.post(
  "/follow",
  async function (req: Request, res: Response, next: NextFunction) {
    const followRes = await FollowModel.query().insert({
      follower_id: req.body.user.id,
      following_id: req.body.following_id,
      created_at: new Date(),
    });
    return res.json(followRes);
  }
);

router.post(
  "/unfollow",
  async function (req: Request, res: Response, next: NextFunction) {
    const deleteResponse = await FollowModel.query()
      .delete()
      .where("follower_id", req.body.user.id)
      .andWhere("following_id", req.body.following_id);
    return res.json(deleteResponse);
  }
);

router.get(
  "/all",
  async function (req: Request, res: Response, next: NextFunction) {
    console.log("reached");
    const users = await LoginInfoModel.query()
      .leftJoin("follows", function () {
        this.on("follows.follower_id", req.body.user.id).andOn(
          "login_info.id",
          "follows.following_id"
        );
      })
      .whereNot("login_info.id", parseInt(req.body.user.id))
      .select([
        "login_info.id",
        "login_info.name",
        raw(
          "CASE WHEN follows.following_id IS NULL THEN 0 ELSE 1 END AS is_following"
        ),
      ]);
    console.log(users);
    return res.json(users);
  }
);

export default router;
