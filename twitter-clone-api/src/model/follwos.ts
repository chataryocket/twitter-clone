import { Model } from "objection";
import knex from "../db-config";

Model.knex(knex);

export default class LoginInfoModel extends Model {
  [x: string]: any;

  static get tableName() {
    return "follows";
  }
}
