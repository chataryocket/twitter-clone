import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

function hashPasswordAsync(pwd: string) {
  return bcrypt.hash(pwd, 10);
}

function comparePasswordAsync(pwd: string, hashPwd: string) {
  return bcrypt.compare(pwd, hashPwd);
}

function createToken(user: any) {
  let signingKey = process.env.JWT_SIGNING_KEY || "";
  return jwt.sign(
    { name: user.name, email: user.email, id: user.id },
    signingKey,
    { expiresIn: "1h" }
  );
}

export { hashPasswordAsync, comparePasswordAsync, createToken };
