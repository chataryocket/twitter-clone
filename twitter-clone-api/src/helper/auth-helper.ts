import StatusCodes from "http-status-codes";
import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

export default function (req: Request, res: Response, next: NextFunction) {
  try {
    var authHeader = req.headers.authorization || "";
    var token = authHeader.split(" ")[1];
    req.body.user = jwt.verify(token, process.env.JWT_SIGNING_KEY || "");
  } catch (err) {
    return res.status(StatusCodes.UNAUTHORIZED).json(err);
  }
  next();
}
