import connection from "../knexfile";
import Knex from "knex";

const env = process.env.NODE_ENV || "development";
export default Knex(connection[env]);
