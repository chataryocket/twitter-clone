# twitter-clone

## Getting started

```
npx express-generator --view=ejs twitter-clone-api
cd twitter-clone-api
npm install
npm i jet-logger helmet http-status-codes command-line-args dotenv module-alias
npm i -D @types/node @types/express @types/cookie-parser @types/morgan @types/http-errors typescript ts-node nodemon @types/fs-extra fs-extra @types/command-line-args tsconfig-paths
```

## About - web

- Vue(Form, Router, Validation, Vuex - State management, Lazy loading, Component based architecture)
- Tailwind css
- Remaining ( Passing props to component, Integrating axios)

## Authors and acknowledgment

Chatar Singh

## License

Open source project.
