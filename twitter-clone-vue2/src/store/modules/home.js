import axios from "../../axios/axios";

const state = () => ({
  tweets: [],
});

// getters
const getters = {};

// actions
const actions = {
  getTweets: function ({ commit }) {
    axios.get("/api/tweet").then(
      (res) => {
        commit("addtweets", res.data);
      },
      (err) => {
        console.log(err);
      }
    );
  },
};

const mutations = {
  addtweets: function (state, tweets) {
    state.tweets = tweets;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
