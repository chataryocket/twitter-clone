const state = () => ({
  users: [],
  loggedInUser: null,
  isAuthenticated: false,
});

// getters
const getters = {
  getLoggedInUser: (state) => state.loggedInUser,
};

// actions
const actions = {
  signup: function ({ commit }, user) {
    commit("addUser", user);
  },
  login: function ({ commit, state }, user) {
    var u = state.users.find(
      (x) => x.email === user.username || x.phone === user.username
    );
    if (u) {
      commit("setLoggedInUser", u);
      commit("setAuth", true);
    } else {
      commit("setLoggedInUser", null);
      commit("setAuth", false);
    }
  },
};

// mutations
const mutations = {
  addUser: function (state, user) {
    state.users.push(user);
  },
  setLoggedInUser: function (state, user) {
    state.loggedInUser = user;
  },
  setAuth: function (state, val) {
    state.isAuthenticated = val;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
