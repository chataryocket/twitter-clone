import axios from "axios";
import router from "../router";

const instance = axios.create({
  baseURL: "http://localhost:3000",
});

instance.interceptors.request.use(
  (config) => {
    var token = localStorage.getItem("token");
    if (token) {
      config.headers["Authorization"] = "bearer " + token;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response.status) {
      router.replace("/");
    }
    return Promise.reject(error);
  }
);

export default instance;
