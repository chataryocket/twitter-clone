import TweetComponent from "../components/home/tweet.vue";
import FollowComponent from "../components/home/follow.vue";

export default [
  {
    path: "",
    component: TweetComponent,
  },
  {
    path: "follow",
    component: FollowComponent,
  },
];
