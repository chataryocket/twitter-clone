import LoginComponent from '../components/Login.vue'
import SignupComponent from '../components/Signup.vue'

export default [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  }
];
