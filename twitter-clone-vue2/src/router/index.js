import Vue from "vue";
import VueRouter from "vue-router";

import LandingPage from "../views/LandingPage.vue";

import AccountRoutes from "./account";
import HomeRoutes from "./home";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "LandingPage",
    component: LandingPage,
    beforeEnter: (to, from, next) => {
      let token = localStorage.getItem("token");
      if (!token) {
        next();
      } else {
        next("/home");
      }
    },
  },
  {
    path: "/home",
    name: "Home",
    component: () => import("../views/Home.vue"),
    children: HomeRoutes,
  },
  {
    path: "/account",
    name: "Account",
    component: () => import("../views/Account.vue"),
    children: AccountRoutes,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
